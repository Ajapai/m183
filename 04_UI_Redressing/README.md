# M183/04_UI_Redressing

## Features

- My page uses the exact same layout as the original as long as the zoom is on 100%.

- The focus on page load is set to my input box instead the one in the iframe.

- Like on the original page it is possible to submit from the password input box via enter.

- After submiting the script will send the input box values to the uiredressing api and redirects the user to the original page after telling him to try again.

## Additional Infos

* I had to use firefox instead of google chrome for this assignement because google chromes cookie policy.
  * In order to use the gibz webpage one has to accept the cookies in the browser. But google does only allow third parties access to those cookies when they are set to 'SameSite=None'.
  *  As of february 2020 google chrome sets 'SameSite=Lax' by default which means cookies are only set when the domain in the URL of the browser matches the domain of the cookie.
  *   Thus in order to use the gibz page 'SameSite' would have explicitly be set to 'none'.

- Next I had to deal with firefoxes 'Same Origin Policy'. It blocked the Cross-Origin Request (namely the api fetch command).
  * This problem was easily solvable after a bit of research. I just had to revert to the less modern but still reliable 'XMLHttpRequest'.
  * This way I was able to call the api without fetching anything thus completing the task given in the assignement.
