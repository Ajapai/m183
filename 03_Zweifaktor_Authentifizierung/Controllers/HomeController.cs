﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Praxisarbeit_2FA.Data;
using Google.Authenticator;
using System.Linq;

namespace Praxisarbeit_2FA.Controllers
{
    public class HomeController : Controller
    {
        private readonly M183DbContext _context;
        private readonly ILogger<HomeController> _logger;

        public HomeController(M183DbContext context, ILogger<HomeController> logger)
        {
            _context = context;
            _logger = logger;

            context.Database.EnsureCreated();
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] string username, [FromForm] string password, [FromForm] string currentKey, [FromForm] string resetKey)
        {

            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);
            if (user == null)
            {
                // User not found - credentials are invalid
                ViewBag.AlertType = "danger";
                ViewBag.Message = "Wrong Credentials";
                return View();
            }

            if (user.Password == password)
            {
                ViewBag.currentUser = username;
                ViewBag.Has2FA = user.Has2FA;
                if(!user.Has2FA)
                {
                    Random random = new Random();
                    user.SecretKey = username + "_" + new string(Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", 10)
                        .Select(s => s[random.Next(s.Length)]).ToArray());
                    _context.SaveChanges();

                    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                    var setupInfo = tfa.GenerateSetupCode("GempiApp", "gempi@eli.com", user.SecretKey, false, 5);
                    string qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                    ViewBag.qrCode = qrCodeImageUrl;
                    ViewBag.secretKey = user.SecretKey;
                }
                else if(ViewBag.qrCode != null)
                {
                    ViewBag.qrCode = null;
                }
                return View();
            }

            if (currentKey != null)
            {
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                if(tfa.ValidateTwoFactorPIN(user.SecretKey, currentKey))
                {
                    if(!user.Has2FA)
                    {
                        user.Has2FA = true;
                        _context.SaveChanges();
                    }
                    return RedirectToAction("UserProfile", new {userId = user.Id});
                }
            }

            if (resetKey == user.SecretKey)
            {
                user.Has2FA = false;
                _context.SaveChanges();
                
                ViewBag.Message = "Successfully reset 2-factor-authentication. Login to set a new one.";
                ViewBag.AlertType = "success";
                return View();
            }

            // User found but incorrect passwort
            ViewBag.Message = "Wrong Credentials";
            ViewBag.AlertType = "danger";
            return View();
        }

        public IActionResult Reset(string username)
        {
            ViewBag.currentUser = username;
            return View();
        }

        public async Task<IActionResult> UserProfile(Guid userId)
        {
            var user = await _context.Users.FindAsync(userId);
            ViewBag.Username = user.Username;
            return View();
        }

    }
}