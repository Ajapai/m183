﻿using System;

namespace Praxisarbeit_2FA.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Has2FA { get; set; }
        public string SecretKey { get; set; }
    }
}
