# M183/03_Zweifaktor_Authentifizierung

## Features

- This application uses google authenticator.

- The app tracks if a user already has a 2FA and initializes the registraiton of a device
    upon a users login (if that user does not already have a 2FA).

- The Secret token for the google authenticator is randomly generated and used to reset the 2FA if the device is lost.
