# M183/02_Keylogger

## Features
- This keylogger toggles between two modes: focused and blurred.
    While in blurred mode it will send the recorded payload everytime "spacebar" is pressed or an input field becomes focused.
    In focused mode it will only send the payload once the field is blurred again.

- The script will keep track of the users position within the input field,
    as long as this is done by keys instead of a mouse click.

- Furthermore the script is able to record all special characters aswell as ignoring all unnessecary inputs like the "F1-12"-keys.
