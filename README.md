# M183
Dieses Repository enthält sämtliche Praxisarbeiten,
welche im Rahmen des Unterrichts im Modul 183 am Gewerblich-industriellen Bildungszentrums Zug (GIBZ) von **Elias Gemperle** bearbeitet wurden.<br/>
Mehr Informationen sind im jeweiligen Readme der Unterordner zu finden.

## 02_Keylogger
Ein Keylogger ist ein Skript oder Programm welches unbemerkt die Tasteninputs des Opfers aufzeichnet und an den Server des Angreifers übermittelt.<br/>
In dieser Praxisarbeit habe ich eine Webseite erstellt welche im Hintergrund einen solchen Keylogger hat.
Normalerweise werden diese aber vom Angreiffer mithilfe verschiedener Techniken auf den Browser des Opfers eingeschleust.

## 03_Zweifaktor_Authentifizierung
Zweifaktor Authentifizierung oder kurz 2FA bezeichnet die zusäzliche Nutzung einer Sicherheitsmasnahme zum Login eines Users.<br/>
Üblicherweise verwendet man Nutzername/Passwort und einen "Possesion factor" wie zb ein Handy um beim Login zusäzlich den an das Handy gesendeten Code zu verifizieren.<br/>
In dieser Praxisarbeit habe ich eine bestehendes Projekt mit "Google Authenticator" erweitert.

## 04_UI_Redressing
Unter UI Redressing versteht man das versteckte Einbetten einer Webseite auf einer anderen Website.<br/>
So kann der Angreifer dem User vormachen dass er sich zb. auf Facebook einloggt um so an seine Daten zu kommen. Es kann aber auch in Form von Clickjacking genutzt werden um likes oder views für die im hintergrund versteckte website zu erhalten.<br/>
In dieser Praxisarbeit habe ich die gibz login page eingebettet und die eingegebenen Daten an eine Api gesendet.

## 05_Sichere_Passwörter
Sichere Passwörter sind immer wieder Thema und es ändert sich auch immer wieder was denn nun empfohlen wird was sichere Passwortpraktiken sind. Fest steht dass Sicherheitsmasnahmen immer in relation zur Anwendung stehen sollten und den User nich mehr als nötig belasten sollten. <br/>
In der Praxisarbeit habe ich eine Webapplikation geschrieben die bei einem Registrierungs Formular klar ersichtlich den User Informiert was er tun muss um ein sicheres Passwort einzugeben.

## 06_Single_signOn
Single Sign On bedeuted, dass man sich mit einem Dienst auf mehreren Webseiten anmelden kann und so sich nur die Zugangsdaten von einem merken muss. Es trägt auch erheblich zur usability bei sich mit einem klick auf mehreren Webseiten einloggen zu können. <br/>
In der Praxisarbeit habe ich ein Projekt so erweitert dass das Login via Google möglich ist, sowie Zweifaktor Authentifizierung um das Single Sign On sicherer zu gestalten.

## 07_HTTP_Digest_Authentication
HTTP Digest Authentication is die Möglichkeit eines webservers userdaten wie name und passwort mit dem Web Browser eines Users auszutauschen. So können Sicherheitskritische Anfragen sicherer gemacht werden indem geprüft wird ob jemand zb eine replay attacke ausführt. <br/>
In der Praxisarbeit habe ich gelernt wie man solche Digest Headers erzeugt sowie welche funktionen die verschiedenen Felder haben.

## 08_Cross_Site_Request_Forgery
Cross Site Request Forgery ist das ungewollte einreichen einer request an einen anderen Dienst wo das Opfer berechtigung hat. Wenn zb. das Opfer bei Google angemeldet ist, kann der Angreifer jegliche requests im namen des Users durchführen. (Sofern dieser auf den vorgefertigten Link auf der vorgefertigten Website klickt um das Script auszuführen) <br/>
In der Praxisarbeit habe ich eine Webseite erstellt die folgende api anspricht https://m183.gibz-informatik.ch/api sofern der user auf m183.gibz-informatik.ch angemeldet ist und ohne sein einferständiss das passwort ändert oder andere Aktionen druchführt.

## 10_Informationsquellen
In diesem Praxisauftrag konnte ich drei verschiedene News Portale mit dem Schwerpunkt Computertechnologie auswählen und analysieren. Zusäzlich habe ich einen Artikel ausgewählt und bin näher auf diesen eingegangen. 
