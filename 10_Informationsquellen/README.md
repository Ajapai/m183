# M183/10_Informationsquellen

## Melani - Nationales Zentrum für Cybersecurity (NCSC)

* Der schweizer Bund bitet über diese Plattform eine Anlaufstelle für alle schweizer Institutionen sowie dem Volk bei Cyberfragen.
    So will der Bund das Thema Cybersecurity verstärkt wahrnehmen und die Schweiz vor Cyberbedrohungen schützen.

* Wenn mir als Benutzer also etwas verdächtiges auf meinem Computer auffällt, kann ich mich via NCSC darüber Informieren sowie meinen Vorall melden.
    Durch die Meldung unterstüzt man den Kampf gegen Cyberattacken.

* Ich als Applikationsentwickler kann die NCSC nutzen um mich über aktuelle Bedrohungen informieren und falls nötig,
    entsprechende Sicherheitsvorkehrungen in meinen Applikationen treffen.

* Das NCSC ist eine zuferlässige und informative Quelle die allgemein verständlich ist. Da sie sich nur auf Nationaler Ebene mit Cyberrisiken auseinandersetzt
    ist sie zwar aktuell aber eher oberflächlich. Dies ist aber gut so, da man so nicht mit Infos über Gefahren überfluten wird, sondern gezielt auf momentan relevante
    Cyberrisiken in der Schweiz aufmerksam gemacht wird.

## Computerworld

* Diese Webpage umfasst mehrere Themengebiete rund um Computertechnik. Uns intressiert in dem Fall nur Security Bereich. Man merkt, dass in vielen Artikeln Fokus auf den wirtschaftlichen
    Nutzen gelegt wird. Sie richtet sich also vor allem an Leute die in Unternehmen für die Cybersecurity zuständig sind. Die Beiträge wirken zudem mehr wie Zeitungsartikel mit
    spannender Schlagzeile und anschliessender Berichterstattung wie man es von Pressebeiträgen halt gewohnt ist.

* Mir persönlich hat diese Quelle nicht viel zu bieten, da ich weder das Presseformat mag noch das wirtschaftliche Intresse besitze das vielen der Beiträge zugrunde liegt.

* Auch als Applikationsentwickler ist diese Seite eher wenig Fachwissen enthält und mehr auf allgemeine informationen setzt.

* Bei Computerworld erhält man mehr unterhaltung als fachspezifisches Wissen präsentiert. Die Artikel sind allgemein verständlich und aktuell aber die relevanz der Themen hängt sehr vom
    Intresse des Lesers ab.

## Hackercombat

* Hackercombat ist eine Cybersecurity News Platform die sich vor allem an IT Experten richtet.
    Zusäzlich zum veröffentlichen von Neuigkeiten bietet sie auch Produkt Reviews an um so die Leute hinter den Projekten zu schulen.

* Als normaler User ist die Website eher nicht attraktiv da sich ein grossteil der Artikel auf sehr spezifische Sicherheitslücken richtet.

* Als Aplikationsentwickler wird die Website schon spannender. Man findet viele Artikel die einem bei der Sicherung einer Software inspirieren und so auch weiterhelfen können.

* Bei Hackercombat handelt es sich also um eine sehr fachspezifische aber informative Quelle. Sie hat teils aktuelle und relevante Beiträge aber man findet auch Beiträge die einfach
    "Nice to know" sind. Das bedeuted aber nicht das diese Beiträge nutzlos oder veraltet sind.

## Ausgewählte Quelle - How Do Random Number Generators Work?

* Dieser Post handelt von RNGs (Random Number Generators) und deren anwendung in online Casinos. Damit das Glücksspiel fair für den Spieler und das Haus ist muss ein RNG
    gewissen Standarts entsprechen, dass dieser nicht gecknackt und wharhaftig 'zufällig' ist. Ob dieser die gewünschten Kriterien erfüllt wird von verschiedenen Organisationen
    geprüft, je nach dem wo der Standort der Firma des online Casinos ist.

* Persönliche Gedanken:
    * Mir war bereits bekannt, dass RNGs mithilfe von ständig wechselnden Seeds ein unvorhersehbares Ergebniss erzeugen können. Doch was mich überaschist, dass es sich bei solchen Seeds
        um bis zu 200'000 stellige Zahlen handeln kann.
    * Der Artikel ist eher etwas oberflächlich und war für mich zwar spannen, jedoch scheinen die darin vorhandenen Infos eher Basiswissen zu sein wenn man in einer Firma arbeitet die
        Glücksspiele entwickelt.
    * Ich werde mich in Zukunft wohl bei Simulationen die zufällige Ereignisse messen soll noch intesiv damit befassen was 'akzteptabler Zufall' in einer digitalen Umgebung ist, jedoch
        hat mir dieser Artikel nicht wirklich aufgezeigt wie man das anstellt. Man könnte aber sagen, dass er mich zu diesem Thema 'sensibilisiert' hat und ich mich in Zukunft
        darauf achte wann ich welche 'intensität' von Zufall nutzen sollte.

_Link zur Quelle: https://hackercombat.com/how-do-random-number-generators-work/_

