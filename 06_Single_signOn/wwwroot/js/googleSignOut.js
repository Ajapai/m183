function signOut() {
    // Enable the auth2 api.
    gapi.load('auth2', function () {
        gapi.auth2.init()
            .then(function () {
                const auth2 = gapi.auth2.getAuthInstance();

                // The authentication api is now available.

                // TODO: Check whether the user is still signed in.

                // TODO: If the user is still signed in --> sign out using the authentication api (auth2).
                // This paragraph might help: https://developers.google.com/identity/sign-in/web/sign-in#sign_out_a_user

                // TODO: After sign-out has completed successfully, call the toggleLogoutState function to indicate the correct state.   
            });
    });
}

/**
 * Toggles the visibility of two opposite paragraphes in the frontend to indicated the current status of the sing-out process.
 */
function toggleLogoutState() {
    document.getElementById('logout-pending').style.display = 'none';
    document.getElementById('logout-complete').style.display = 'block';
}

// When this file is loaded by the browser, the signOut function will be called.
signOut();

