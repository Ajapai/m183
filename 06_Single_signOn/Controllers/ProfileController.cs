using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SingleSignOn.Models;
using Google.Authenticator;

namespace SingleSignOn.Controllers
{
    public class ProfileController : Controller
    {
        /**
         * The user manager is a handy tool for various operations regarding User objects.
         * It might be used to create and/or retrieve users.
         */
        private readonly UserManager<User> _userManager;

        /**
         * Constructor of the ProfileController.
         * The userManager dependency is provided by dependency injection.
         */
        public ProfileController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        /**
         * Displays the users profile data.
         * The Authorize attribute protects this action: Only authenticated user may call it.
         */
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Index([FromForm] string currentKey)
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.User = user;
            AuthentificationViewModel model = new AuthentificationViewModel();

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            if(!user.TwoFactorEnabled)
            {
                if(tfa.ValidateTwoFactorPIN("mySuperSecretKey", currentKey))
                {
                    await _userManager.SetTwoFactorEnabledAsync(user, true);
                    return View();
                }
                if(!string.IsNullOrEmpty(currentKey))
                {
                    model.ErrorMessage = "Sorry, wrong code, please scan the image again and try again.";
                }
                model.QrCodeImageUrl = tfa.GenerateSetupCode("GempiApp", "gempi@eli.com", "mySuperSecretKey", false, 5).QrCodeSetupImageUrl;
            }
            else if(tfa.ValidateTwoFactorPIN("mySuperSecretKey", currentKey))
            {
                return View();
            }
            else if(!string.IsNullOrEmpty(currentKey))
            {
                model.ErrorMessage = "Sorry, wrong code, try again.";
            }

            return View("TwoFactorAuthenticaton", model);
        }

        /**
         * Logout handler - ends the currently authenticated users session.
         */
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return View();
        }
    }
}