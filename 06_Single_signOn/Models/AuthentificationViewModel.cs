using System;

namespace SingleSignOn.Models
{
    public class AuthentificationViewModel
    {
        public string ErrorMessage { get; set; }
        public string QrCodeImageUrl { get; set; }
    }
}