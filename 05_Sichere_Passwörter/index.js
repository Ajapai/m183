// Global fields for user and hashed password
let dbuser = "";
let dbpassword = "";

// Toggle function
$('.message a').click(function(){
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});

// Event management
$('.register-form input[type=password]').bind('input focus blur', function(event){
    switch(event.type)
    {
        case "focus":
            showChecklist();
            break;

        case "input":
            checkValue(event.target.value);
            break;

        case "blur":
            hideChecklist();
            break;

        default:
            break;
    }
});

// Validation display
let password_ok = false;
let active_element = null;

function showChecklist() {
    if(active_element != null)
    {
        active_element.animate({height: "0px"}, "slow");
    }
    if(!password_ok)
    {
        active_element = $('#checklist');
        active_element.animate({height: "129px"}, "slow");
    }
    else{
        active_element = $('#check_ok');
        active_element.animate({height: "34px"}, "slow");
    }
};

function hideChecklist() {
    active_element.animate({height: "0px"}, "slow");
    if(password_ok)
    {
        active_element = null;
    }
    else
    {
        active_element = $('#check_not_ok');
        active_element.animate({height: "34px"}, "slow");
    }
};

// Validation handling
let has_number = false;
let has_lenght_8 = false;
let has_lenght_12 = false;
let has_symbol = false;
let has_caseing = false;

function checkValue(value) {
    let temp_bool = /\d/.test(value);
    if (temp_bool != has_number)
    {
        toggleClass($('#number_check')[0]);
        has_number = temp_bool;
    }

    temp_bool = value.length >= 12;
    if (temp_bool != has_lenght_12)
    {
        toggleClass($('#length_12_check')[0]);
        has_lenght_12 = temp_bool;
        has_lenght_8 = has_lenght_12;
    }

    if (!temp_bool)
    {
        temp_bool = value.length >= 8;
        if(temp_bool != has_lenght_8)
        {
            toggleClass($('#lenght_8_check')[0]);
            has_lenght_8 = temp_bool;
        }
    }

    temp_bool = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(value);
    if (temp_bool != has_symbol)
    {
        toggleClass($('#symbol_check')[0]);
        has_symbol = temp_bool;      
    }

    temp_bool = /^(?=.*[A-Z])(?=.*[a-z])/.test(value);
    if (temp_bool != has_caseing)
    {
        toggleClass($('#caseing_check')[0]);
        has_caseing = temp_bool;      
    }

    temp_bool = (has_number | 0) + (has_lenght_8 | 0) + (has_lenght_12 | 0) + (has_symbol | 0) + (has_caseing | 0) >= 4;
    if(temp_bool != password_ok)
    {
        password_ok = temp_bool;
        showChecklist();
    }
};

function toggleClass(element) {
    if(element.className == "valid")
    {
        element.classList.remove('valid');
        element.classList.add('invalid');
    }
    else if(element.className == "invalid")
    {
        element.classList.remove('invalid');
        element.classList.add('valid');
    }
};


// Encryption
const bcrypt = dcodeIO.bcrypt;

function encryptPassword(value){
    return bcrypt.hashSync(value, bcrypt.genSaltSync(value.length));
}

// Register and Login
function register(event)
{
    let form = event.target.children;
    if(!password_ok || form[0].value == "" || form[2].value == "")
    {
        alert('Bitte füllen sie alle Felder korrekt aus.');
        return false;
    }

    dbuser = form[0].value;
    dbpassword = encryptPassword(form[2].value);

    form[0].value = "";
    form[1].value = "";
    form[2].value = "";

    alert('Successfully registered!\nYour Password hash is: ' + dbpassword);
    $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    return false;
}

function login(event){
    let form = event.target.children;

    if(form[0].value == dbuser && form[1].value != "" && bcrypt.compareSync(form[1].value, dbpassword)){
        form[0].value = ""; 
        form[1].value = ""; 
        alert('Successfully logged in. (if there would be a user page...)')
        return false;
    }

    form[1].value = ""; 
    alert('Incorrect username or password');
    return false;
}
