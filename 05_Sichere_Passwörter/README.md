# M183/05_Sichere_Passwörter

## Features

- The Application contains a login form and a register form which can be toggled.

- When entering a password during the registering process, the app will track the validity of the password and inform the user about the current state.

- If the user wants to submit an empty or invalid form a popup will inform the user and inputs won't be reset.

- Upon registration the password will be hashed via bcrypt and stored in a global variable called `dbpassword`.

## Thoughts and explanations

- In my validation process I wanted to find a middleway between usability and security.

* I found a way to make it easier for the user to choose a password whilst also making it harder for brute force attackers:
    * This was accomplished by making one of the five validation requirements optional but the user can choose which one.
    * If all requirements would be necessary a potential attacker could rule out all password combinations where any of the requirements is not given.
    * In my case an attacker still has to check all combinations that do not contain a special character or number for example thus making it a bit harder.

- For the encryption of the passwords I was using bcrypt.
    * It uses special hashes that contain the randomly generated salt inside the hash value thus preventing rainbow table attacks.
    * The hash would then be stored in the database and everytime a user wants to login bcryt can compare the given password value with the stored hash.

## Known Bugs

* For some reason the bcrypt compare method always returns false and I could not figure out why or how to use it diffrently.
<br/>
P.S. I spent way to much time on ui design and figuring out how to use regex. Still, usefull stuff to know. :astonished:
